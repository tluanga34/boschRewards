<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, PATCH, DELETE');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Headers: X-Requested-With,content-type');

    $data = array(
        "xAxis" => array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"),
        "yAxis" => array()
    );

    $retailer = array(
        "label"     => "Dealer",
        "color"     => "#75b844",
        //"values"    => array(3000,6000,5000,300,4000,2000,3212,4321,122,3222,2111,1268)
        "values"    => array(0,0,0,0,0,0,0,0,0,0,0,0)
    );

    $subretailer = array(
        "label"     => "Sub Dealer",
        "color"     => "#00a9b8",
        //"values"    => array(3222,6000,6666,3333,2144,7642,7321,7624,122,322,433,100)
        "values"    => array(0,0,0,50,0,0,0,0,0,0,0,0)
    );

     $user = array(
        "label"     => "User",
        "color"     => "#0098d7",
        //"values"    => array(3222,6000,6666,3333,2144,7642,7321,7624,122,322,433,100)
        "values"    => array(0,0,0,0,0,0,0,0,0,0,0,0)
    );

    array_push($data['yAxis'],$retailer);
    array_push($data['yAxis'],$subretailer);
    array_push($data['yAxis'],$user);

    sleep(1);
    echo json_encode($data);

?>