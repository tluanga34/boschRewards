<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, PATCH, DELETE');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Headers: X-Requested-With,content-type');

    $data = array(
        "xAxis" => array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"),
        "yAxis" => array()
    );

    $earned = array(
        "label"     => "Earned",
        "color"     => array("#ba0379","#860558","#5a073d"),
        "values"    => array(3000,6000,5000,2000,4000,3000,3212,4321,2500,3222,2111,5000)
    );

    $burned = array(
        "label"     => "Burned",
        "color"     => array("#8e177f","#61267a","3d3277"),
        "values"    => array(2000,4000,2000,1500,3000,2500,2212,1321,2000,2222,1111,4200)
    );



    array_push($data['yAxis'],$earned);
    array_push($data['yAxis'],$burned);
    

    sleep(1);
    echo json_encode($data);

?>