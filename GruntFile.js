module.exports = function(grunt) {
	
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-contrib-cssmin");

	grunt.initConfig({
		concat: {
	    	js: {
	     		src: [
					 "libs/d3.v4.min.js",
					 "libs/jquery-2.2.3.min.js",
					 "libs/angular.min.js",
					 "libs/angular-route.js",
					 "libs/dhtml_calendar/dhtmlxcalendar.js",
					 "js/app.js",
					 "js/appconfig/dashboards_page.js",
					 "js/filters/filters.js",
					 "js/controllers/parent.js",
					 "js/controllers/login.js",
					 "js/controllers/index_page.js",
					 "js/controllers/infoPages.js",
					 "js/controllers/dashboards_home.js",
					 "js/controllers/dash_user_registrations.js",
					 "js/controllers/dash_user_redemptions.js",
					 "js/controllers/dash_sku_scanned.js",
					 "js/controllers/dash_points_earned_vs_burned.js",
					 "js/service/auth.js",
					 "js/service/constructor.js",
					 "js/service/dashboards_data.js",
					 "js/service/graphConstructors.js",
					 "js/directives/loading.js",
					 "js/directives/set-height.js",
					 "js/directives/area-chart1.js",
					 "js/directives/date-picker.js",
				],
	    		dest: "dist/dashboard.pages.js",
	    	},
			staticPages : {
				src: [
					 "libs/angular.min.js",
					 "libs/angular-route.js",
					 "js/app.js",
					 "js/appconfig/static_pages.js",
					 "js/filters/filters.js",
					 "js/controllers/parent.js",
					 "js/controllers/login.js",
					 "js/controllers/index_page.js",
					 "js/controllers/infoPages.js",
					 "js/service/auth.js",
					 "js/service/constructor.js",
				],
	    		dest: "dist/static.pages.js",
			},
	    	css : {
	    		src : [
					"libs/bootstrap/css/bootstrap.min.css",
					"css/style.css"
				],
	    		dest : "dist/main.css"
	    	}
	 	},
	 	uglify: {
			targets: {
	    		files: {
	    			"dist/dashboard.pages.min.js": ["dist/dashboard.pages.js"],
	    			"dist/static.pages.min.js": ["dist/static.pages.js"],
	    		}
			}
		},
		cssmin: {
		 	options: {
				mergeIntoShorthands: false,
		   		roundingPrecision: -1
		  	},
		  	target: {
		    	files: {
		      		"dist/main.min.css": ["dist/main.css"]
		    	}
		  	}
		}
	});

	grunt.registerTask("default", ["concat","uglify","cssmin"]);
}

