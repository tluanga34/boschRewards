//DECLARGIN ALL DIRECTIVES IN THIS SECTION
app.directive("ngLoading",function(){
	return {
		template : "<table class='pop-up fontGrey' ng-class='(ngLoading.state)?\"active\":\"\"' ng-cloak><tr><td class='text-center'><div style='max-width:400px; margin:0px auto'><div class='text-right'><a href='javascript:void(0)' ng-click='ngLoading.hide()'><small>X</small></a></div><br>Please wait..<br><p ng-bind='ngLoading.msg'></p></div></td></tr></table>",
		scope : {
			ngLoading : '='
		},
		link : function(scope, $elem){
			////console.log($elem);
		}
	}
});
