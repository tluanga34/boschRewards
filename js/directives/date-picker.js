//DIRECTIVE FOR INPUT DATE FUNTIONALITIES
app.directive("ngInputDate",[function(){
	return {
		restrict : "A",
		scope : {
			ngInputDate : "=",
		},
		link : function(scope, $elem, $attr){
						
			var model = scope.ngInputDate,
                id = model.id,
				dateElem;

			$elem.attr({"id":id});
				
			dateElem = new dhtmlXCalendarObject(id);
			dateElem.setDateFormat("%d-%M-%Y");
			dateElem.hideTime();
			dateElem.setDate(new Date(model.value));

			
			
			$elem.val(dateElem.getFormatedDate("%d-%M-%Y", new Date(model.value)));
			
			//METHOD TO SHOW THE CALENDAR
			model.show = function(){
				
				//PUTTING TIMEOUT SO TO PREVENT IMMEDIATE HIDE BY EVENT BUBBLING
				setTimeout(function(){
					dateElem.show();
				},10);
				
			}

            //EXPOSE API TO MODEL
            model.setInsensitiveRange = function(from, to){
                dateElem.setInsensitiveRange(from, to);
            }

            model.setSensitiveRange = function(from, to){
                dateElem.setSensitiveRange(from, to);
            }
						
			//LISTEN TO USER SELECT DATE EVENT AND UPDATE MODEL ACCORDINGLY
			dateElem.attachEvent("onClick", function(date, state){
				model.value = dateElem.getDate();
                if(model.onChange)
                    model.onChange();
				scope.$apply();
			});
			
			//LISTENING TO OBJECT RESET REQUEST AND EMPTY THE INPUT FIELD
			scope.$watch('ngInputDate.value', function(newValue, oldValue) {
				if(newValue == "" || newValue == undefined){
					$elem.val("");
					
					if($elem.attr("required") != undefined)
						$elem[0].setCustomValidity("Please pick a date");
				} else{
					$elem[0].setCustomValidity("");
				}
			});

            model.onLoad();
				
		}
	}
}]);
