app.directive("ngAreachart1", function() {
    return {
        scope: {
            ngAreachart1: "="
        },
        link: function(scope, $elem, $attr) {

            var timeout = null
              , firstLoad = true;

            scope.$watch("ngAreachart1.xAxis", function(newData) {

                //Flagging the first load and make some delay so that, the SVG Canva's width calculation will be accurate.//
                //console.log("Chart change");
                if (firstLoad) {
                    clearTimeout(timeout);
                    timeout = setTimeout(function() {
                        drawGraph(scope.ngAreachart1);
                        firstLoad = false;
                    }, 100);
                } else {
                    drawGraph(scope.ngAreachart1);
                }

            });

            window.addEventListener("resize", function() {
                clearTimeout(timeout);
                timeout = setTimeout(function() {
                    drawGraph(scope.ngAreachart1);
                }, 50);

            });

            function drawGraph(data) {
                //console.log(data);
                $elem.empty();
                //$elem[0].innerHTML = "";

                if (data == null || data == undefined || data == '' || data.xAxis ==  undefined) {
                    //console.log("Null data value in chart directive");
                    return;
                }

                var dim = {
                    width: $elem.width(),
                    height: $elem.height(),
                    topMargin: 30,
                    interval: $elem.width() / (data.xAxis.length),
                    leftStart: $elem.width() * 0.02,
                };

                function Coord(x, y) {
                    this.x = x || "";
                    this.y = y || "";
                }

                var chart = d3.select($elem[0])
                  , path = ''
                  , xAxisHeight = 70
                  , axisHeight = dim.height - xAxisHeight - dim.topMargin
                  , y = ''
                  , x
                  , qx
                  , qy
                  , prevX = ''
                  , prevY = '';


                //console.log(data);

                //Lineear Gradient Defs
                var gradientContainer = chart.append("defs")
                    .selectAll("linearGradient").data(data.yAxis).enter().append("linearGradient")
                    .attr("id", function(d){return d.label}).attr("x1", "0%").attr("y1", "0%")
                    .attr("x2", "0%").attr("y2", "100%")
                    .selectAll('stop').data(function(d){return d.color.slice(0, (d.color.length - 1))}).enter().append('stop')
                    .attr("offset",function(d,i){
                        return (i == 0)? '43%':'98%';
                    })
                    .attr("style",function(d){
                        return "stop-color:" + d + ";stop-opacity: 1";
                    });

                //Draw bottom box for labels//
                var bottomGroup = chart.append("g").attr("class", "xAxis-group");
                bottomGroup.append("rect").attr("width", dim.width).attr("height", xAxisHeight).attr("x", 0).attr("y", axisHeight + dim.topMargin).attr("fill", "#e7e8ea");

                //Drawing bottom labels
                bottomGroup.selectAll("text").data(data.xAxis).enter().append("text").attr("x", function(d, i) {
                    //console.log(i);
                    return dim.interval * (i) + dim.leftStart;
                }).attr("y", (dim.height - 40)).text(function(d, i) {
                    return d
                });

                //Draw the path for data//
                var pathGroup = chart.append('g').attr("class", "path-group");

                //Create group for points label//
                var pointsDotGroup = chart.append('g').attr("class", "points-dot-group");
                var pointsGroup = chart.append('g').attr("class", "points-group");

                var xStart = dim.leftStart + 10;
                var yStart = axisHeight + dim.topMargin;

                pathGroup.selectAll("path").data(data.yAxis).enter().append("path").attr("fill", function(d) {
                    //attr("fill", "url(#gradient)");
                    return "url(#"+d.label+")"; // d.color[0];
                }).attr("d", function(d, i) {
                    //console.log(d);

                    d.pointHeight.forEach(function(key, index) {
                        if (index == 0) {
                            path = "M " + xStart + " " + yStart;
                            //console.log(path);
                        }

                        //console.log(key);
                        y = axisHeight - (key / 100 * axisHeight) + dim.topMargin;

                        //console.log(y);
                        x = (dim.interval * index + xStart);

                        if(index > 0){	
							qx = x - ((x - prevX) / 2 );
							qy = (y == yStart && prevY == yStart)? y : (index % 2 == 0)? y + 10 : y - 10;
							//console.log(prevY== yStart);
                            path += " Q "+ qx +" "+ qy +" " + x + " " + y;
                            
                        } else
                            path +=" L "+x+" "+y;

                        
                        if (index == (d.values.length - 1)) {
                            path += " L " + x + " " + yStart;
                        }

                        //console.log(d.color[2]);

                        pointsDotGroup.append('circle').attr('r', 5).attr('cx', (dim.interval * index + dim.leftStart + 10)).attr('cy', y).attr("class", d.label);

                        var textNode = pointsGroup.append('text').attr("class", "arial").attr("fill", d.color[2]).attr('x', (dim.interval * index + dim.leftStart + 10)).attr('y', function() {
                            if (i % 2 != 0)
                                return y + 20;
                            else
                                return y - 15;
                        }).text(d.pointsLabel[index]);

                        var textWidth = textNode._groups[0][0].getBBox().width;

                        if(textWidth && index != 0 ){
                            if(index != d.pointHeight.length - 1)
                                textNode.attr('x',((dim.interval * index + dim.leftStart + 10) - (textWidth /2)));
                            else
                                textNode.attr('x',((dim.interval * index + dim.leftStart + 10) - textWidth));
                        }

                        prevY = y;
                        prevX = x;


                    });

                    //console.log(path);

                    return path;
                });

            }
        }
    }
});