app.service("Const",[function(){

   var _const = this;
   //DEFINING CONSTRUCTOR AND PROTOTYPE FUNCTIONS FOR ALL TYPES OF TOGGLING MODELS
    _const.Data = function(defaultState){
        this.state 	= defaultState || false;
        this.msg	= '';
        this.value = '';
	    this.list = [];
	    this.options = null;
        this.id = '';
    };

    _const.Data.prototype.show = _const.Data.prototype.enable = function(msg){
        if(!this.state)
            this.state = true;
        //console.log(msg);
        if(msg != undefined)
            this.msg = msg;
    };

    _const.Data.prototype.hide = _const.Data.prototype.disable = function(){
        if(this.state)
            this.state = false;

        if(this.msg != '')
            this.msg = '';
    };
    _const.Data.prototype.toggle = function(){
        this.state = !this.state;
    };

    _const.Data.prototype.setList = function(data){
        this.list = data;
    };

    _const.Data.prototype.setValue = function(data){
        this.value = data;
    };

    _const.Data.prototype.setId = function(id){
        this.id = id;
    }

    _const.Data.prototype.setOptions = function(data){
        this.options = data;
    };

    _const.Data.prototype.setListForModel = function(data){
        var _self = this;
        _self.list = [];

        data.forEach(function(key,index){
            _self.list.push({
                label : key,
                value : key.toLowerCase()
            });
        });

        _self.list.unshift({
            label : "All",
            value : "all"
        });
    }

}]);