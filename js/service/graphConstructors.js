Array.prototype.numMax = function(){
	var max = 0;
	for(var i = 0; i < this.length; i++){
		if(max < this[i])
			max = this[i];
	}
	return max;
}

app.service("dashConst",["$filter",function($filter){

    var _dashConst = this;

    //CONSTRUCTOR CLASS FOR SIMPLE 2D GRAPH
    _dashConst.twoDGraph = function(){};

    //PROTOTYPE FUNCTION FOR SIMPLE 2D GRAPH
    _dashConst.twoDGraph.prototype.init = function(graphData){
            
        var _self = this;

        _self.xAxis = graphData.xAxis;
        _self.yAxis = graphData.yAxis;
        _self.totalValue   = 0;
        _self.peakValue = 0;
        _self.baseValue = _self.yAxis[0];

        _self.yAxis.forEach(function(key,index){
            if(key > _self.peakValue)
                _self.peakValue = key;

            if(key < _self.baseValue)
                _self.baseValue = key;

             _self.totalValue += key;
        });
    }

    //CONSTRUCTOR CLASS FOR MULTI DIMENSIONAL GRAPH
    _dashConst.multiDGraph = function(){};

    //PROTOTYPE FUNCTION FOR MULTI DIMENSIONAL GRAPH
    _dashConst.multiDGraph.prototype.init = function(graphData){
        var _self = this;

        _self.xAxis = graphData.xAxis;
        _self.yAxis = graphData.yAxis;
        _self.totalValue   = 0;
        _self.peakValue = 0;
        _self.baseValue = _self.yAxis[0];

        _self.yAxis.forEach(function(key,index){

            key.totalValue = key.peakValue = 0;
            key.baseValue = key.values[0];

            key.values.forEach(function(key2, index2){
                if(key.peakValue < key2)
                    key.peakValue = key2;
                
                if(key.baseValue > key2)
                    key.baseValue = key2;
                
                key.totalValue += key2;
            });


        });
    }

    //CONSTRUCTOR FOR AREA CHART
    _dashConst.areaChart = function(){};

    _dashConst.areaChart.prototype.init = function(graphData){

        //console.log( $filter('toK')(10000));

        var _self = this;

        _self.xAxis = graphData.xAxis;
        _self.yAxis = graphData.yAxis;
        


        _self.peakValue = 0;

        var peakValue = [];

        _self.yAxis.forEach(function(key,index){
            key.totalValue = key.values.numMax()
            peakValue.push(key.totalValue);
        });

        _self.peakValue = peakValue.numMax();


        _self.yAxis.forEach(function(key,index){
            key.pointHeight = [];
            key.pointsLabel = [];
            key.values.forEach(function(key2, index2){
                key.pointHeight.push(Math.round(key2 / _self.peakValue * 100));
                key.pointsLabel.push($filter('toK')(key2));
            });
        });


    }

}]);