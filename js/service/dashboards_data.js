app.service("dashData",["$http",function($http){

    var _dashData = this;

    //THIS WILL STORE THE JSON DATA AND WON'T NEED TO CALL SERVER EVERY TIME.
    _dashData.store = {};

    _dashData.getTile = function(callback){

        if(_dashData.store.tileData != undefined){
            callback(_dashData.store.tileData);
        }
        else {
            $http({
                url : config.api.getDashTiles,
                method : "GET",
            })
            .then(
                function(response) {
                    //CACHE THE RESPONSE DATA
                    _dashData.store.tileData = response.data;
                    callback(response.data);
                },
                function(failed){
                    
                }
            );
        }
    };

    _dashData.getUserRegistrations = function(params, callback){
        
        $http({
            url : config.api.getUserRegistrations,
            method : "GET",
            params : params
        })
        .then(
            function(response) {
                //CACHE THE RESPONSE DATA
                callback(response.data);
            },
            function(failed){
                
            }
        );
    };

    _dashData.getUserRedemptions = function(params, callback){
        

        $http({
            url : config.api.getUserRedemptions,
            method : "GET",
            params : params
        })
        .then(
            function(response) {
                //CACHE THE RESPONSE DATA
                callback(response.data);
            },
            function(failed){
                
            }
        );
    };

    
    _dashData.getSkuScanned = function(params, callback){
        

        $http({
            url : config.api.getSkuScanned,
            method : "GET",
            params : params
        })
        .then(
            function(response) {
                //CACHE THE RESPONSE DATA
                callback(response.data);
            },
            function(failed){
                
            }
        );
    };

    
    _dashData.getSkuScannedUser = function(params, callback){
        

        $http({
            url : config.api.getSkuScannedUser,
            method : "GET",
            params : params
        })
        .then(
            function(response) {
                //CACHE THE RESPONSE DATA
                callback(response.data);
            },
            function(failed){
                
            }
        );
    };
    
    _dashData.getPointsEarnedvsBurned = function(params, callback){
        

        $http({
            url : config.api.getPointsEarnedvsBurned,
            method : "GET",
            params : params
        })
        .then(
            function(response) {
                //CACHE THE RESPONSE DATA
                callback(response.data);
            },
            function(failed){
                
            }
        );
    };


    

}]);