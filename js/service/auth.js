app.service("Auth",["$http",function($http){

    var _auth = this;

    _auth.request = function(user, callback){

        console.log(user);

        $http({
            url : config.api.login,
            method : "POST",
            data : {
                email : user.email,
                password : user.password
            }
        }).then(
            function(response){
                callback(response.data);
            },
            function(failed){
                callback({error : "Failed due to technical error."});
            }
        );

    }


}]);