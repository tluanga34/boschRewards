//NUMBER THAT SCALE VALUES TO PERCENTAGE BASED ON THE MAX VALUE
app.filter("scaleUpToPercent",[function(){
    var localStore = {};
    return function(number, maxNum){
       
        if(number != undefined && maxNum != undefined && !isNaN(number) && !isNaN(maxNum)){
            
            //CALCULATE THE NUMBER AND CACHE IT FOR NEW INPUTS.
            if(localStore[number] == undefined || localStore[number][maxNum] == undefined){
                //console.log("cache it");
                localStore[number] = {};

                localStore[number][maxNum] = Math.round(number/maxNum * 100) | 0;
                return localStore[number][maxNum];
            } 
            //RETURN THE CACHED NUMBER IF ALREADY CACHED
            else {
               //console.log("return cached");
                return localStore[number][maxNum];
            }
            
        }
            
    }
}]);

//FILTER TO CONVERT NUMBERS WITH THOUSANDS TO K
app.filter("toK",[function(){
    
    return function(number, param){

        if(param == 'dashzero' && number <= 0){
            return "-";
        }
        else if(param == 'blankzero' && number <= 0){
            return "";
        }
        else if(number != undefined && !isNaN(number)){
            return (number < 1000)? number : (number / 1000).toFixed(1) + 'k';
        }
            
    }
}]);

//RETURN NUMERIC VALUE TO INDIAN COMMA SEPARATOR SYSTEM
app.filter("indianNum", function () {
    return function (input, param) {
        
        if(input == undefined || input == '' || isNaN(input))
            return;
        else if (input == 0)
            return '-';
        else if (input < 1000 && input >= -1000) {
            if (param == "2decimal")
                return input.toFixed(2);
            else
                return Math.round(input)
        }

        
        var output = (parseInt(input)).toString(),
            decimal,
			start = 3,
			pos;

        decimal = input.toString()
        if (decimal.indexOf(".") != -1) {
            decimal = decimal.split(".")[1].split("").splice(0, 2).join("");
            decimal = "." + decimal;
        } else {
            decimal = ".00";
        }

        output = output.split('');

        var isMinus = false;

        if (output[0] == '-') {
            output.shift();
            isMinus = true;
        }

        pos = (output.length - 3)

        for (; pos > 0; pos -= 2) {
            output.splice(pos, 0, ',');
        }

        if (isMinus)
            output.unshift('-');

        output = output.join('');

        if (param == "2decimal")
            return output + decimal;
        else
            return output;
    }
});