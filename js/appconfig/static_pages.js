//CONFIGURING THE ROUTING FUNCTIONALITY
app.config(["$routeProvider",function($routeProvider){

	//REGISTERING ROUTES FOR ALL PAGE SECTIONS
	config.staticRoutes.forEach(function(key){

		$routeProvider.when(key.routeUrl, {
			templateUrl : key.templateUrl,
			controller : key.controller,
			 resolve : {
			 	pageSection : function(){
			 		return key.section;
			 	}
			 }
		});
	});

}]);