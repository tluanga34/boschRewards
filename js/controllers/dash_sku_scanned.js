
app.controller("SkuScanned",["$scope","$route","Const","dashData","dashConst",function(s,$route,Const,dashData,dashConst){
	
    //CONSTRUCTING 2D GRAPH USING CONSTRUCTOR
    s.data = new dashConst.multiDGraph();

    //CONSTRUCTING MODELS FOR DATA FILTERS
    s.regionFilter      = new Const.Data(false);
    s.fromDateFilter    = new Const.Data(false);
    s.toDateFilter      = new Const.Data(false);

    //FETCHING ITEMS TO FILTER MODELS
    s.regionFilter.setListForModel(config.dataFilters.regions);
    s.regionFilter.setValue(s.regionFilter.list[0]);

    s.fromDateFilter.setId("filterFrom");
    s.toDateFilter.setId("filterTo");

    s.fromDateFilter.setValue(new Date(config.dataFilters.filterStartDefault));
    s.toDateFilter.setValue(new Date());

   s.fromDateFilter.onLoad = function(){
       s.fromDateFilter.setSensitiveRange(new Date(config.dataFilters.campaignStart), s.toDateFilter.value);
   }

   s.toDateFilter.onLoad = function(){
       s.toDateFilter.setSensitiveRange(s.fromDateFilter.value, new Date());
   }

   s.fromDateFilter.onChange = function(){
        //console.log("onchange");
        s.toDateFilter.setSensitiveRange(s.fromDateFilter.value, new Date());
        s.getData();
   }

   s.toDateFilter.onChange = function(){
        s.fromDateFilter.setSensitiveRange(new Date(config.dataFilters.campaignStart), s.toDateFilter.value);
        s.getData();
   }

    

    //USE DASHDATA SERVICE AND CALL BACKEND DATA
    s.getData = function(){

        //SHOW LOADING DASHBOARDS TEXT TO USERS
        s.$parent.loading.show("Loading dashboards data");

        dashData.getSkuScanned({
            region : s.regionFilter.value.value,
            dateFrom : s.fromDateFilter.value.getTime(),
            dateTo : s.toDateFilter.value.getTime()
        },function(responseData){
            s.data.init(responseData);
            s.$parent.loading.hide();
        });
    }

    s.getData();
    
    

}]);


app.controller("SkuScannedUser",["$scope","$route","Const","dashData","dashConst",function(s,$route,Const,dashData,dashConst){
	
    //CONSTRUCTING 2D GRAPH USING CONSTRUCTOR
    s.data = new dashConst.multiDGraph();

    //CONSTRUCTING MODELS FOR DATA FILTERS
    s.regionFilter      = new Const.Data(false);
    s.fromDateFilter    = new Const.Data(false);
    s.toDateFilter      = new Const.Data(false);

    //FETCHING ITEMS TO FILTER MODELS
    s.regionFilter.setListForModel(config.dataFilters.regions);
    s.regionFilter.setValue(s.regionFilter.list[0]);

    s.fromDateFilter.setId("filterFrom_user");
    s.toDateFilter.setId("filterTo_user");

    s.fromDateFilter.setValue(new Date(config.dataFilters.campaignStart));
    s.toDateFilter.setValue(new Date());

   s.fromDateFilter.onLoad = function(){
       s.fromDateFilter.setSensitiveRange(new Date(config.dataFilters.campaignStart), s.toDateFilter.value);
   }

   s.toDateFilter.onLoad = function(){
       s.toDateFilter.setSensitiveRange(s.fromDateFilter.value, new Date());
   }

   s.fromDateFilter.onChange = function(){
        //console.log("onchange");
        s.toDateFilter.setSensitiveRange(s.fromDateFilter.value, new Date());
        s.getData();
   }

   s.toDateFilter.onChange = function(){
        s.fromDateFilter.setSensitiveRange(new Date(config.dataFilters.campaignStart), s.toDateFilter.value);
        s.getData();
   }

    

    //USE DASHDATA SERVICE AND CALL BACKEND DATA
    s.getData = function(){

        //SHOW LOADING DASHBOARDS TEXT TO USERS
        s.$parent.loading.show("Loading dashboards data");

        dashData.getSkuScannedUser({
            region : s.regionFilter.value.value,
            dateFrom : s.fromDateFilter.value.getTime(),
            dateTo : s.toDateFilter.value.getTime()
        },function(responseData){
            s.data.init(responseData);
            s.$parent.loading.hide();
        });
    }

    s.getData();
    
    

}]);