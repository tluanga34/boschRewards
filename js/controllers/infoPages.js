app.controller("InfoPages",["$scope","$route","pageSection","Const",function(s,$route,pageSection,Const){
	

	s.mainSection 	= new Const.Data(false);
	s.log 			= console.log;
	s.navigations 	= config.staticNavigations;

	//s.log(pageSection);
	s.$parent.pageSection = pageSection;



	s.$on("$routeChangeSuccess", function(event, current, previous){
		//console.log(current);
    	s.activeRoute = current.loadedTemplateUrl;
    	s.$parent.lastVisited = (pageSection == 'aboutProgram')? "#"+current.$$route.originalPath : s.$parent.lastVisited;
    	s.mainSection.show();
  	});


}]);