app.controller("LoginController",["$scope","Auth","Const",function(s,Auth,Const){
	
	//CONSTRUCTING MODELS
	s.loginForm	= new Const.Data(false);
	s.submitBtn	= new Const.Data(true);
	s.formMsg	= new Const.Data(false);

	//METHOD FOR FORM SUBMIT
	s.loginForm.submit = function(event){
		
		event.preventDefault();
		s.submitBtn.disable();
		s.formMsg.hide();

		//CALLING REQUEST METHOD IN THE AUTH SERVICE // SENDING EMAIL AND PASSWORD
		Auth.request({
			email : this.email,
			password : this.password
		}, function(response){
			
			s.submitBtn.enable();
			
			//DISPLAY ERROR MESSAGE IF THERE IS ERROR KEY IN THE RETURN JSON
			if(response.error != undefined){
				console.log(response.error);
				s.formMsg.show(response.error);
				console.log(s.formMsg);
			} else {
				//IF NOT ERROR, NAVIGATE TO DASHBOARDS PAGE	
				location.assign("dashboards.html");
			}

		});
	}

}]);
