app.controller("DashHome",["$scope","$route","dashData",function(s,$route,dashData){
	
    s.tileData = [];

    s.$parent.loading.show("Loading dashboards data");
    dashData.getTile(function(responseData){
        s.tileData = responseData;
        s.$parent.loading.hide();
    });

}]);