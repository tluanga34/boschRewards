var config = {}

//Path configurable at one place
//config.dataPath = "http://localhost/boschRewards/bosch_web/demoserver/"
config.dataPath = "./demoserver/"

//LIST OF APIs

config.api = {
	login : config.dataPath+"session.php",
	getDashTiles : config.dataPath+"getDashTileData.php",
	getUserRegistrations : config.dataPath+"getUsersRegistrations.php",
	getUserRedemptions : config.dataPath+"getUserRedemptions.php",
	getSkuScanned : config.dataPath+"getSkuScanned.php",
	getSkuScannedUser : config.dataPath+"getSkuScanned_user.php",
	getPointsEarnedvsBurned : config.dataPath+"getPointsEarnedvsBurned.php",
};

//Overwriting previous APIs
// config.api = {
// 	login : config.dataPath+"session.php",
// 	getDashTiles : "https://api.myjson.com/bins/8dman",
// 	getUserRegistrations : "https://api.myjson.com/bins/9z2yv",
// 	getUserRedemptions : "https://api.myjson.com/bins/giumf",
// 	getSkuScanned : "https://api.myjson.com/bins/14xmfb",
// 	getSkuScannedUser : "https://api.myjson.com/bins/no1vr",
// 	getPointsEarnedvsBurned : "https://api.myjson.com/bins/pihuv",
// };

config.dataFilters = {
	// Campaign Start Date: MM/DD/YYYY
	campaignStart : "01/01/2017",
	// From filter default value will set to current year Jan
	filterStartDefault : "01/01/"+(new Date()).getFullYear(),
	regions : ["East","West","North","South"]
};

config.dashboardRoutes = [
	{
		name : "Dashboard Home",
		routeUrl : "/",
		templateUrl : "templates/dashboards/home.html",
		controller : 'DashHome',
		section : ''
	},
	{
		name : "User Registrations",
		routeUrl : "/userregistrations",
		templateUrl : "templates/dashboards/user-registrations.html",
		controller : 'DashUserReg',
		section : ''
	},
	{
		name : "User Redemptions",
		routeUrl : "/userredemptions",
		templateUrl : "templates/dashboards/user-redemptions.html",
		controller : 'DashUserRedemption',
		section : ''
	},
	{
		name : "SKU Scanned",
		routeUrl : "/skuscanned",
		templateUrl : "templates/dashboards/sku_scanned.html",
		controller : '',
		section : ''
	},
	{
		name : "Points Earned vs Burned",
		routeUrl : "/earnedvsburned",
		templateUrl : "templates/dashboards/point_earned_vs_burned.html",
		controller : 'EarnedvsBurned',
		section : ''
	},

];

config.staticRoutes = [
	{
		name : "Login Form",
		routeUrl : "/login",
		templateUrl : "templates/login_form.html",
		controller : 'LoginController',
		section : ''
	},
	{
		name : "Forget Password Form",
		routeUrl : "/forgetPassword",
		templateUrl : "templates/forget_password.html",
		controller : 'LoginController',
		section : ''
	},
	{
		name : "Reset Password Form",
		routeUrl : "/resetPassword",
		templateUrl : "templates/reset_password.html",
		controller : 'LoginController',
		section : ''
	},

	{
		name : "Customers Program Details",
		routeUrl : "/customersProgramDetails",
		templateUrl : "templates/customers_programdetails.html",
		controller : 'InfoPages',
		section : 'aboutProgram'
	},
	{
		name : "Dealers Program Details",
		routeUrl : "/dealersProgramDetails",
		templateUrl : "templates/dealers_programdetails.html",
		controller : 'InfoPages',
		section : 'aboutProgram'
	},
	{
		name : "Tearms & Conditions for Dealers",
		routeUrl : "/dealerterms",
		templateUrl : "templates/dealers_terms.html",
		controller : 'InfoPages',
		section : 'aboutProgram'
	},
	{
		name : "Tearms & Conditions for Customers",
		routeUrl : "/customerterms",
		templateUrl : "templates/customers_terms.html",
		controller : 'InfoPages',
		section : 'aboutProgram'
	},

	{
		name : "Contact Us",
		routeUrl : "/contactCs",
		templateUrl : "templates/contact_customers.html",
		controller : 'InfoPages',
		section : 'contactUs'
	},
	{
		name : "Contact Us",
		routeUrl : "/contactDl",
		templateUrl : "templates/contact_dealers.html",
		controller : 'InfoPages',
		section : 'contactUs'
	},


];

config.staticNavigations = {
	customers : [
		{
			label : "Program Details",
			routeURl : "#customersProgramDetails",
			templateUrl : "templates/customers_programdetails.html",
		},
		{
			label : "Terms & Conditions",
			routeURl : "#customerterms",
			templateUrl : "templates/customers_terms.html",
		},

	],
	dealers : [
		{
			label : "Program Details",
			routeURl : "#dealersProgramDetails",
			templateUrl : "templates/dealers_programdetails.html",
		},
		{
			label : "Terms & Conditions",
			routeURl : "#dealerterms",
			templateUrl : "templates/dealers_terms.html",
		},

	]
};