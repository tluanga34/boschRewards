## Configurations

- API URLs are stored in site_config.js. 
- All routes are done from the front end. Route paths are stored in site_config.js
- Dashboard dates and region filters are based from declared objects in site_config.js
- By Default, date fillter will range from config.dataFilters.campaignStart to current date (site_config.js).
- Region filter list will populated from config.dataFilters.regions array  (site_config.js).
- Controllers, Service, and filters have their own folder. Each are separated in their own file as well. 


### CONCATENATION AND MINIFICATION
- Concatenated and minified js/css files are stored in dist folder.
- While developing, use the unconcat one.
- Static pages like Index.html, infoPages.html has a different js from dashboards.html. You can see that inside dist folder

### Dependency Management
- NPM is in used to install dependency for gruntjs
- GruntJS is used to automate concat and min tasks.